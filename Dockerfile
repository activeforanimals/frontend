FROM node:9.6.1

ADD data/ /usr/src/app/

ENV PATH /usr/src/app/node_modules/.bin:$PATH

WORKDIR /usr/src/app

RUN \
  npm install; \
  npm install react-scripts@1.1.1 -g;

CMD ["npm", "run web"]
